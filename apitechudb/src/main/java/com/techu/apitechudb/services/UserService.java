package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository ;

    public List<UserModel> findAll() {
        System.out.println("findAll UsersService");

        return this.userRepository.findAll();
    }

    public List<UserModel> findAll(String orderBy) {
        System.out.println("findAll UsersService");

        List<UserModel> result;
        if(orderBy != null){
            System.out.println("Se ha pedido ordenacion");
            result = this.userRepository.findAll(Sort.by("age"));
        }else {
            result = this.userRepository.findAll();
        }

        return result;
    }



    public UserModel add(UserModel user){
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id){
        System.out.println("En FindById de UserService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel userModel){
        System.out.println("Update en UserService");

        return this.userRepository.save(userModel);

    }

    public boolean delete(String id){
        System.out.println("delete en UserService");
        boolean result = false ;

        if (this.findById(id).isPresent() == true ){
            System.out.println("User encontrado borrando");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;

    }


}
