package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository ;

    public List<ProductModel> findAll() {
        System.out.println("findAll ProductService");

        return this.productRepository.findAll();
    }

    public ProductModel add(ProductModel product){
        System.out.println("add en ProdcutService");

        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("En FindById de ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel productModel){
        System.out.println("Update en ProductService");

        return this.productRepository.save(productModel);

    }

    public boolean delete(String id){
        System.out.println("delete en PoductService");
        boolean result = false ;

        if (this.findById(id).isPresent() == true ){
            System.out.println("Producto encontrado borrando");
            this.productRepository.deleteById(id);
            result = true;
        }

        return result;

    }

}
