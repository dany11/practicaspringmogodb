package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService ;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "order_by", required = false ) String sortBy ){
        System.out.println("getUsers");
        System.out.println("Los user enlistados se ordenaran " + sortBy);


        return new ResponseEntity<>(
                this.userService.findAll(sortBy),
                HttpStatus.OK
        );

    }


    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del user a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);


        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "User no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del user que se va a crear es " + user.getId());
        System.out.println("El nombre del user que se va a crear es " + user.getName());
        System.out.println("La edad del user que se va a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user)
                , HttpStatus.CREATED
        );

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateProduct(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del user a actualizar en parametro URL es " + id);
        System.out.println("La id del user a actualizar es " + user.getId());
        System.out.println("El name del user a actualizar es " + user.getName());
        System.out.println("La edad del user a actualizar es " + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()){
            System.out.println("User para actualizar encontrado, actualizando");
            this.userService.update(user);
        }

        return new ResponseEntity<>(
                user
                , userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "User borrado" : "User no Borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }






}
